class Header{
    constructor(text){
        this.text = text;
    }
}

class Node{
    constructor(id, type, name){
        this.id = id
        this.type = type
        this.name = name
    }
}

class Edge{
    constructor(from,to){
        this.source = from
        this.target = to 
    }
}

class Solution{
    constructor(nodes, links, name){
        this.links = links
        this.nodes = nodes
        this.name = name
    }
}

export{Header, Solution, Node, Edge}