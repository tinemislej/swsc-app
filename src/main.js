import Vue from 'vue'
import axios from 'axios'
import eventbus from './bus.js'
import graphvue from './graph.vue'
import fileUploadVue from './fileupload.vue'
import errPopup from './alert.vue'
import fca from './failedcompositionalert.vue'
import solutionsVue from './suggestions.vue'
import { Header, Solution, Node, Edge } from './models.js'
import { Z_VERSION_ERROR } from 'zlib';
import {GET_GRAPH_URL, ADD_SWAGGER_URL} from './config.js'

var appVue = new Vue({
  el: '#app',
  components: {
    'graph': graphvue,
    'alert': errPopup,
    'solutionz':solutionsVue,
    'fileupload': fileUploadVue,
    'failedalert': fca
  },
  data: {
    outputs: '',
    inputs: '',
    loading: false,
    graphData: {},
    candidates:[],
    onlyOneCandidate: true,
    suggestions:[],
    error: 'none'
  },
  methods: {
    postData: function () {
      var self = this
      var trimmedInputs = this.inputs.replace(/\s/g, "")
      var trimmedOutputs = this.outputs.replace(/\s/g, "")
      var outputz = trimmedOutputs.split(",")
      var inputz = trimmedInputs.split(",")
      this.loading = true
      this.suggestions = []
      this.candidates = []
      this.onlyOneCandidate = true
      this.error = "none"
      axios.post(GET_GRAPH_URL, {
        "inputs":inputz,
        "outputs":outputz
      })
        .then(function (response) {
          self.graphData = response
          self.loading = false
          var cand = self.parseData(JSON.parse(response.data))
          self.candidates = cand
          self.onlyOneCandidate = self.candidates.length <= 1
          eventbus.$emit('dataChanged',self.candidates[0])
        })
        .catch(function (error) {
          
          if (!error.response){
            self.error = "general"
          } else {
            var code = error.response.status
            if(code == 404){
              self.error ="failed-composition"
              var response = JSON.parse(error.response.data)
              var m = self.parseError(response.solutions)
              self.suggestions = m
              eventbus.$emit('dataChanged',m[1]) 
            }else{
              self.error = "general"
            }
        }

          self.loading = false
        })
    },
    parseData: function(data){
          var candidates = []
          data.candidates.forEach(function(candidate){
              var edges = []
              candidate.edges.forEach(function(edge){

                  var sourceNode = candidate.nodes.findIndex(function(node){
                      return node.id === edge.from
                  })

                  var targetNode = candidate.nodes.findIndex(function(node){
                          return node.id === edge.to
                  })

                  var link = {source: sourceNode,target: targetNode}
                  edges.push(link)
              })

              candidates.push({
                  score: candidate.score,
                  nodes: candidate.nodes,
                  links: edges
              })
          })
          return candidates
    },
    showSuggestions:function(){
      return this.suggestions.length > 0
    },
    onCandidateClick: function(idx){
      eventbus.$emit('dataChanged', this.candidates[idx])
     },
    parseError: function(data){
      var models = []
      data.forEach(function(l){
          var pairingsCount = l[0].pairings.length

          var header
          if(pairingsCount == 1){
            header = '1-pairing solutions'
          }else{
            header = pairingsCount + "-pairings solutions"
          }

          models.push(new Header(header))
          l.forEach(function(solution){
              var idx = 0
              var nodes = []
              var edges = []
              solution.pairings.forEach(function(pairing){
                var inputId = idx++
                var epId = idx++
                var outputId = idx++
                  var from = new Node(inputId, "parameter", pairing.from.mainConcept)
                  var ep = new Node(epId, "endpoint", "??")
                  var to = new Node(outputId, "parameter", pairing.to.mainConcept)

                  nodes.push(from)
                  nodes.push(ep)
                  nodes.push(to)

                  edges.push(new Edge(inputId, epId))
                  edges.push(new Edge(epId, outputId))
              })

              models.push(new Solution(nodes, edges, "A solution"))
          })
      })

      return models
    }
  }
})
