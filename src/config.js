const HOST = 'http://localhost:8080'
export const GET_GRAPH_URL = HOST + '/getGraph'
export const ADD_SWAGGER_URL = HOST + '/addSwagger'